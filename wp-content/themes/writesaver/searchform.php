<?php
/**
 * Template for displaying search forms in writesaver
 *
 * @package WordPress
 * @subpackage writesaver
 * 
 */
?>


<form role="search" method="get" class="search_box" action="<?php echo get_site_url(); ?>">
    <input required="" type="text" class="tftextinput1" placeholder="<?php echo esc_attr_x('Search here ', 'placeholder', 'writesaver'); ?>" value="<?php echo get_search_query(); ?>" name="s" id="search"/>
    <input type="submit" class="tfbutton1" value="">	
</form>


