<?php
/*
 * Template Name: blog
 */
add_filter('wp_head', 'prew_next_rel_link_wp_head_new', 10, 0);
get_header();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="main_blog">
        <div class="blog_category_sticky">
            <div class="container">
                <div class="blog_category_sticky_right">
                    <div class="search_box">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="blog_category_sticky_left">
                    <div class="desktop_catagory">
                        <div class="blog_category_full_list">
                            <div class="category_btn">
                                <div class="category_btn_icon">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                                </div>
                                <div class="category_btn_txt">
                                    <span>Categories</span>
                                </div>
                            </div>
                            <?php
                            global $terms;
                            $terms = get_terms(array(
                                'post_type' => 'post',
                                'taxonomy' => 'category',
                                'hide_empty' => false,
                                'number' => 5
                            ));
                            if ($terms):
                                echo '<ul id="links">';
                                $count = 0;
                                foreach ($terms as $term) {
                                    $count++;
                                    echo '<li id="' . $term->slug . '"  class="' . $class . '">' . $term->name . '</li>';
                                }
                                echo '</ul>';
                            endif
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blog_category_main" id="notContent">
            <div class="container">
                <?php foreach ($terms as $term) { ?>
                    <div id="<?php echo $term->slug; ?>" class="blog_listt">
                        <?php
                        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => 10,
                            'paged' => $paged,
                            'category_name' => $term->slug,
                            'category_id' => $term->id
                        );
                        $the_query = new WP_Query($args);
                        $imgIter=1;
                        if ($the_query->have_posts()) :
                            while ($the_query->have_posts()) : $the_query->the_post();
                                ?>

                                <div class="blog_block">
                                    <div class="blog_img">
                                        <img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="img-responsive" alt="<?titleImg(); echo $imgIter;?>" title="<?titleImg(); echo $imgIter;?>" >
                                        <div class="blog_textblock">
                                            <div class="blog_txt">
                                                <div class="blog_title">
                                                    <h4>
                                                        <?php
                                                        $len = strlen(get_the_title());
                                                        if ($len > 80):
                                                            echo substr(get_the_title(), 0, 80) . "...";
                                                        else:
                                                            echo get_the_title();
                                                        endif;
                                                        ?>
                                                    </h4>
                                                </div>
                                                <div class="blog_date">
                                                    <span><?php echo get_the_date(); ?></span>
                                                </div>
                                                <div class="blog_desc">
                                                    <?php the_excerpt(); ?>

                                                </div>
                                                <div class="blog_social">
                                                    <?php echo do_shortcode('[TheChamp-Sharing ]') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hover_effect_blog">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="hover_effect_blog_img">
                                                <img src="<?php echo get_template_directory_uri() ?>/images/sky_arrow.png" class="img-responsive">

                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <?php $imgIter++;?>
                            <?php endwhile; ?>
                            <div class="pagination_main">
                                <?php
                                wp_reset_postdata();
                                if (function_exists(custom_pagination)) {
                                    custom_pagination($the_query->max_num_pages, "", $paged);
                                } else {
                                    _e('Sorry, no posts matched your criteria.');
                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
<script>
    if (typeof (localStorage) === 'undefined') {
        document.getElementById("result").innerHTML =
                'Your browser does not support HTML5 localStorage. Try upgrading.';
    } else {
        console.log(localStorage);
    }
    var slug = "";
    $(document).ready(function () {

        $(".pagination").click(function (e) {

            var slug = $('ul#links li.active').attr('id');
            localStorage.setItem('slug', slug);
            return true;
        });
        var slug = localStorage.getItem('slug');
        if (slug == null) {
            $('ul#links li:first').addClass('active');
        }
        else
        {
            $('ul#links li').removeClass('active');
            $('ul#links li#' + slug + '').addClass('active');
            $('#notContent .blog_listt').css('display', 'none');
            $('#notContent #' + slug + '').css('display', 'block');
        }
    });

    localStorage.removeItem(slug);

    $('ul#links li').click(function (e) {
        e.preventDefault();
        debugger;
        // localStorage.removeItem(slug);
        localStorage.setItem('slug', $(this).attr("id"));

        window.location.href = '<?php echo get_page_link(12); ?>';
    });
</script>