<?php
/*
  Template Name: Reset Password
 */
get_header();
?>
<style>
    .form-error
    {
        color: red;
        float: left;
        display:none;
        width: 100%;
        margin-top: 5px;
    }
    #reset_pw span
    {
        float: left;
        font-weight: normal;
    }
    .remember {
        text-align: left;
        float: left;
        width: 100%;
    }
</style>    
<?php
if (is_user_logged_in()) {
    echo '<script>window.location.href="' . site_url() . '"</script>';
    exit;
}
$string = $_REQUEST['string'];
$encrypt_method = "AES-256-CBC";
$secret_key = 'This is my secret key';
$secret_iv = 'This is my secret iv';
// hash
$key = hash('sha256', $secret_key);
// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
$iv = substr(hash('sha256', $secret_iv), 0, 16);
$decrypted_string = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
if (email_exists($decrypted_string)) {
    $user = get_user_by('email', $decrypted_string);
    $user_id = $user->ID;
    $email = $user->user_email;
    $pass = md5($user->user_pass);
    ?>

    <section class="login">
        <div class="breadcum">
            <div class="container">
                <div class="page_title">
                    <h1>Reset Password</h1>
                </div>
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="login_form">
                            <form id="reset_pw" method="post" >
                                <input  type="hidden"  id="login_url" value="<?php echo the_permalink(545); ?>" />
                                <input  type="hidden"  name="email" id="email" value="<?php echo $email; ?>" />
                                <input  type="hidden"  name="oldpass" id="oldpass" value="<?php echo $pass; ?>" />
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
                                <input type="hidden" name="uname" id="uname" value="<?php echo $username; ?>" />

                                <div class="form_footer_input">
                                    <input type="password" name="password" id="password" placeholder=" New Password" class="contact_block"/>
                                </div>   
                                <div class="form_footer_input">
                                    <input type="password" name="conf_password" id="conf_password" placeholder="Confirm Password" class="contact_block" />
                                </div>  
                                <div class="reset_msg"</div>
                                <div class="sub_btn">
                                    <input type="submit" class="btn_sky" value="Submit" id="reset_pass">   
                                </div>
                            </form>                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
} else {
    echo '<p>There is no account associated with this email address.</p>';
}
?>
<script>
    jQuery(document).ready(function () {
        $("#reset_pw").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'text-danger reg_msg', // default input error message class  

            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    pwcheck: true
                },
                conf_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password",
                    pwcheck: true
                    
                }

            },
            messages: {

                password: {
                    required: "Password is required.",
                    minlength: jQuery.validator.format("Please enter at least 6 characters."),
                    pwcheck: "Password must contain both uppercase and lowercase letters, a number and a special character"
                },
                conf_password: {
                    required: "Password confirmation is required.",
                    pwcheck: "Password confirmation must contain both uppercase and lowercase letters, a number and a special character",
                    equalTo: "Passwords do not match",
                    minlength: jQuery.validator.format("Please enter at least 6 characters.")
                }

            },
            submitHandler: function (e) {
                $('#loding').show();
                $('.reg_msg').remove();
                var password = document.getElementById('password').value;
                var conf_password = document.getElementById('conf_password').value;
                var user_id = document.getElementById('user_id').value;
                var email = document.getElementById('email').value;
                var uname = document.getElementById('uname').value;
                var oldpass = document.getElementById('oldpass').value;


                var password_count = password.length;
                var reset_pw = 0;
                $.ajax({
                    type: "POST",
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {
                        action: 'reset_pw_ajax',
                        password: password,
                        user_id: user_id,
                        email: email,
                        oldpass: oldpass
                    },
                    dataType: "html",
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 1)
                        {
                            $('.reset_msg').html('<span  class="text-success reg_msg" >Password successfully updated.</span>');
                            $('.reset_msg').show();
                            window.setTimeout(function () {
                                $('.reg_msg').fadeOut('slow');
                                window.location.href = $('#login_url').val();
                            }, 3000);
                        } else if (data == 0)
                        {
                            $('.reset_msg').html('<span  class="text-danger reg_msg" >There has been a problem, and your password has not been updated. Please try again.</span>');

                            window.setTimeout(function () {
                                $('.reg_msg').fadeOut('slow');
                            }, 3000);
                        }
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }

                });
                return false;

            }
        });
        $.validator.addMethod("pwcheck", function (value) {
            return /^[A-Za-z0-9\d!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/.test(value)
                    && /[A-Z]/.test(value)
                    && /[a-z]/.test(value)
                    && /\d/.test(value)
                    && /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);

        });
    });
</script>
<?php
get_footer();
