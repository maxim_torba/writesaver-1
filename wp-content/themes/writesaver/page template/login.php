<?php
/*
 * Template Name: login
 */
if (is_user_logged_in()):
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
endif;
get_header();
?>
<style>
    span.login_msg
    {
        float: left;
    }
    .remember {
        text-align: left;
        float: left;
        width: 100%;
    }
</style>
<section class="login">

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <h1>Login</h1>

            </div>

            <div class="row">

                <div class="col-sm-offset-3 col-sm-6">

                    <div class="login_form">

                        <form name="signin" id="signin" method="post" action="#">

                            <h3>Sign In </br></br></h3>

                            <input type="email" placeholder="Email Address*" class="contact_block" name="email" required="">

                            <input type="password" placeholder="Password*" class="contact_block" name="pw" required="">
                            <div class="remember">
                                <input name="remember" id="remember" type="checkbox">
                                <label for="remember">Remember me</label>
                            </div>
                            <a href="<?php echo get_the_permalink(747); ?>">Forgot Password?</a>

                            <div class="sub_btn">                                        

                                <input type="submit" class="btn_sky" value="Login">    

                            </div>                                    
                            <div class="msg" id="signin_msg" ></div>
                        </form>

                        <div class="other_login">

                            <span>or</span>

                        </div>

                        <div class="social_login">

                            <?php echo do_shortcode('[TheChamp-Login]'); ?>
                        </div>
                         <p>
                            If you do not have an account click here to <a href="javascript:void(0);" class="open_login" onclick="open_loginpop('register');">Create an account</a>

                        </p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

<?php get_footer(); ?>

<script>

    jQuery(document).ready(function () {
        $(".login_form #signin").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'text-danger login_msg', // default input error message class         
            rules: {
                email: {
                    required: true,
                    email: true
                },
                pw: "required"
            },
            messages: {
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email."
                },
                pw: {
                    required: "Password is required."
                }
            },
            submitHandler: function (form) {
                $('.login_msg').remove();
                $('#loding').show();
                var alldata = $('.login_form #signin').serialize();

                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    type: "POST",
                    data: alldata + '&action=user_signin',
                    //dataType: "html",
                    success: function (data) {
                        debugger
                        if (data == 0)
                        {
                            $('.login_form #signin_msg').html('<span  class="text-danger login_msg" >Your username and password do not match.</span>');
                            setTimeout(function () {
                                $('.login_form .login_msg').fadeOut('slow');
                            }, 1000);
                        } else
                        {
                            $('.login_form #signin_msg').html('<span  class="text-success login_msg" >Login successful</span>');
                            setTimeout(function () {
                                $('.login_form .login_msg').fadeOut('slow');
                                window.location.href = data;
                            }, 1000);
                        }
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
                return false;
            }
        });
    });
</script>