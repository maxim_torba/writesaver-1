<?php
/*
 * Template Name: help_center
 */

get_header();
?>    <section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Help center</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="help_center"> 
        <div class="wrapper-sticky">
            <div class="blog_category_sticky help_list">
                <div class="container">
                    <?php
                    global $terms;
                    $terms = get_terms(array(
                        'taxonomy' => 'help_category',
                        'hide_empty' => false,
                        'orderby' => 'ID',
                    ));
                    if ($terms):
                        echo '<ul id="links">';
                        $count = 0;
                        foreach ($terms as $term) {
                            $count++;
                            echo '<li id="' . $term->slug . '"  class="' . $class . '">' . $term->name . '</li>';
                        }
                        echo '</ul>';
                    endif
                    ?>
                </div>
            </div>
        </div>
        <div class="help_center_blockmain">
            <div class="container">
                <?php foreach ($terms as $term) { ?>
                    <div class="help_center_block">
                        <div class="accordion">

                            <?php
                            $args = array(
                                'post_type' => 'help',
                                'posts_per_page' => 10,
                                'order' => 'ASC',
                                'help_category_name' => $term->slug,
                                'help_category_id' => $term->id
                            );

                            $my_query = new WP_Query($args);
                            if ($my_query->have_posts()) :
                                while ($my_query->have_posts()) : $my_query->the_post();

                                    the_title('<h3>', '</h3>');
                                    //print_r($args);
                                    ?>
                                    <div>
                                        <?php the_content(); ?>
                                    </div>
                                    <?php wp_reset_postdata(); ?>   <?php endwhile; ?>  
                            </div>
                        <?php endif; ?> 
                    </div>
                <?php } ?> 
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>