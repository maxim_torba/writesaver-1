<?php
/*
 * Template Name: document_proofreader_word_tracking
 */

get_header();
global $wpdb;

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "customer") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

$main_doc_id = $_REQUEST['doc'];
if (empty($main_doc_id)) {
    echo '<script>window.location.href="' . get_the_permalink(762) . '"</script>';
    exit;
}
$fk_cust_id = get_current_user_id();
$customer_info = $wpdb->get_results(" SELECT * FROM `tbl_customer_general_info` WHERE fk_customer_id = $fk_cust_id LIMIT 1 ");

$remaining_word_credits = 0;
$total_submited_docs = 0;

if (count($customer_info) > 0) {
    $remaining_word_credits = $customer_info[0]->remaining_credit_words;
    $total_submited_docs = $customer_info[0]->total_submited_docs;
}
$docs = '';

$complted_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_main WHERE fk_customer_id = $fk_cust_id  AND status=1 ORDER BY pk_document_id DESC ");

$main_doc = $wpdb->get_row("SELECT * FROM wp_customer_document_main WHERE pk_document_id=$main_doc_id AND fk_customer_id=$fk_cust_id  AND Status = 1");
?>
<style>
    .doc_desc  p{padding:0 !important;color:inherit !important; font-size:inherit !important;line-height: inherit !important}
    .doc_desc strong{font-family:arial;}
    ins { text-decoration: none; }
</style>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</section>
<section class="uploaded_file_main">
    <div class="clickable">
        <a href="javascript:void(0);" class="openre">View all completed documents <i class="fa fa-angle-down" aria-hidden="true"></i></a>                
    </div>
    <div class="collapsible_content" style="display: none;">
        <div class="inner_content submitted_docs">
            <div class="container-fluid">
                <div class="collapsible_slider">
                    <div class="create_new_doc">
                        <button type="button" id="btnCreateNewDoc" onclick="fnCreateNewDoc()">Create new</button>
                    </div>
                    <div class="dashboard_content_slider">
                        <?php if ($complted_docs): ?>
                            <h5>All completed documents</h5>
                            <div class="dashboard_collapsible_slider">
                                <ul class="slides">
                                    <?php
                                    $doc_count = 0;
                                    foreach ($complted_docs as $value) {

                                        $sub_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` WHERE fk_doc_main_id= $value->pk_document_id AND is_active = 1");
                                       $doc_count = 0;
                                       foreach ($sub_documents as $sub_doc) {
                                        $sub_document_id = $sub_doc->pk_doc_details_id;
                                        if ($sub_doc->status == 'Completed') {
                                            $doc_count++;
                                        }
                                                   }

                                        if (count($sub_documents) == $doc_count) {
                                            ?>
                                            <li data-id="<?php echo $value->pk_document_id; ?>" >
                                                <div class="full_content">
                                                    <div class="full_content_header">
                                                        <h5><?php echo $value->document_title; ?></h5>
                                                    </div>
                                                    <div class="status">
                                                        <p>Status : <span>Completed</span></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        <?php else: ?>
                            <h5>You have not yet submitted any documents.</h5>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="clickable">
                <a href="javascript:void(0);" class="closer">Close <i class="fa fa-angle-up" aria-hidden="true"></i></a>                
            </div>
        </div>                
    </div>
</section>
<section>
    <div class="container">
        <div class="privacy customer proofreader">
            <div class="row service">
                <div class="col-sm-5">
                    <div class="total_ammount credit">
                        <div class="left">
                            <h4><?php echo $remaining_word_credits; ?><span>Words</span></h4>
                            <p>Remaining credit<a href="<?php echo get_page_link(14); ?>">Upgrade Plan</a></p>                                   
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="total_ammount submitted">
                        <div class="left">
                            <h4><?php echo $total_submited_docs; ?><span>Docs</span></h4>
                            <p>Total submitted<a href="javascript:void(0);" id="OpenAllSubmittedDocuments" >View all</a></p>                                
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
                <script>
                    function get_Diff(){
                    	if($('#get-docx-link').length){
                    		return;
                    	}else{
                        $('#get_diff').text('Preparing...');
                        var diff_elem = '';

                        var problem_elem,
                        extract_child = '';
                        if($('.diff > .diff').length){
                          $('.diff > .diff').each(function() {
                            problem_elem = $(this).parent();
                            //console.log(problem_elem);
                            extract_child = $(problem_elem).find('.diff');
                            $(problem_elem).after(extract_child);
                          })
                        }

                        $('.doc_desc .track_result').each(function(){
                            diff_elem += $(this).html();    
                        });
                        $.ajax({
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            data: {action:'get_Diff',
                            diff: diff_elem,
                        	docid: <?php echo $main_doc_id; ?>,
                        	customer: <?php echo $fk_cust_id; ?>,
                        	},
                            type: 'post',
                            success: function (data) {
                                $('#get_diff').text('Done.');
                                $('#get_diff').html('<a id="get-docx-link" style="color:#fff;" target="_blank" href="/wp-content/themes/writesaver/documents/doc-u<?php echo $fk_cust_id; ?>d<?php echo $main_doc_id; ?>.docx">Download .docx</a>');
                            },
                        });
                    	}
                    }
                </script>
                <div class="col-sm-2">
                    <label class="cloud_file download-icon" id="get_diff" onclick="get_Diff()">
                        Prepare .docx
                    </label>
                </div>
                <style>
                    .download-icon a:hover{text-decoration: none;}
                </style>
            </div>
            <?php
            if ($main_doc):
                $main_doc_id = $main_doc->pk_document_id;
                $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id=$main_doc_id  AND Status=1");
                ?>
                <div class="doc_name">
                    <h2><?php echo $result_maindoc_name; ?></h2>
                </div>
                <div class="main_editor">
                    <div class="editor_top">
                        <div class="editor_inner_top">                            
                            <div class="delete_track">

                                <div class="ios_checkbox ios_checkbox_sm">                                    
                                    <input type="checkbox" class="ios8-switch" id="track_chk">                                    
                                    <label for="track_chk">Track Changes</label>
                                </div>
                            </div>
                            <div class="used_word" id="maindocword">
                                <span>Word Count:</span><span class="count">0</span>
                            </div>                            
                        </div>
                        <div class="hidden_scroll">
                            <div class="changeable" contenteditable="false" id="txt_area_upload_doc"<?php echo $sub_document_id; ?> data-id="<?php echo $sub_document_id; ?>" style="height:500px; width: 100%; display: inline-block;    white-space: pre-line;">
                                <?php
                                $debug_originalArr = [];
                                $debug_proofArr = [];
                                $all_doc = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details WHERE fk_cust_id = $fk_cust_id AND fk_doc_main_id= $main_doc_id    ");
                                foreach ($all_doc as $doc) {
                                    $sub_document_id = $doc->fk_doc_details_id;
                                    $debug_original = $wpdb->get_var("SELECT document_desc FROM wp_customer_document_details WHERE pk_doc_details_id=$sub_document_id  AND is_active=1");
                                    $debug_proof = $doc->double_doc_desc;

                                    $debug_originalArr[] = $debug_original;
                                    $debug_proofArr[] = $debug_proof;
                                    ?>
                                    
                                    <?php
                                }
                                $debug_proof = implode(' ', $debug_proofArr);
                                $debug_original = implode(' ', $debug_originalArr);

                                $debug_original = str_replace(' style=\"font-size: inherit;\"', '', $debug_original);
                                $debug_proof = str_replace(' style=\"font-size: inherit;\"', '', $debug_proof);

                                $debug_original = str_replace('<span>', '', $debug_original);
                                $debug_original = str_replace('</span>', '', $debug_original);
                                $debug_proof = str_replace('<span>', '', $debug_proof);
                                $debug_proof = str_replace('</span>', '', $debug_proof);
                                $debug_original = stripslashes($debug_original);
                                $debug_proof = stripslashes($debug_proof);

                                $debug_diff = diffHtml($debug_original, $debug_proof);

                                $debug_diff = stripslashes($debug_diff);

                                $debug_diff = str_replace(' style="display: none;"', '', $debug_diff);
                                ?>
                                <div class="doc_desc">
                                    <div class="origin_desc" style="display: none;"><?php echo $debug_original ?></div>
                                    <div class="double_desc" ><?php echo $debug_proof; ?></div>
                                    <div class="track_result" style="display: none;"><?php echo $debug_diff; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?> 
                <div class="doc_name">
                    <h2>No updated document available.</h2>
                </div>
            <?php endif; ?> 
        </div>
    </div>
</section>
<div id="debug">
    <?php
        // $all_doc = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details WHERE fk_cust_id = $fk_cust_id AND fk_doc_main_id= $main_doc_id    ");
        // foreach ($all_doc as $doc) {
        //     $sub_document_id = $doc->fk_doc_details_id;
        //     $debug_original = $wpdb->get_var("SELECT document_desc FROM wp_customer_document_details WHERE pk_doc_details_id=$sub_document_id  AND is_active=1");
        //     if(preg_match('/<\/p>$/', $debug_original)  && !preg_match('/^<p>/', $debug_original)){
        //         $debug_original = '<p>'.$debug_original;
        //     }
        //     $debug_proof = $doc->double_doc_desc;
        //     if(preg_match('/<\/p>$/', $debug_original) && !preg_match('/^<p>/', $debug_proof)){
        //         $debug_original = '<p>'.$debug_original;
        //     }
        
    ?>
    <div class="original">
        <?php //echo $debug_original; ?>
    </div>
    <div class="proof">
        <?php //echo $debug_proof; ?>
    </div>
    <?php //} ?>
</div>
<script src="<?php echo get_template_directory_uri() ?>/js/diff_new.js"></script>
<script>
    $(document).ready(function () {
        $('#track_chk').change(function () {

            var check_status = this.checked;

            if (check_status == true) {
                $('.track_result').show();
                $('.double_desc').hide();
            } else {
                $('.track_result').hide();
                $('.double_desc').show();
            }

        });
        var words = $.trim($('.double_desc').text()).replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length;
        $(".used_word .count").text(words);
    });
</script>
<?php get_footer(); ?>
