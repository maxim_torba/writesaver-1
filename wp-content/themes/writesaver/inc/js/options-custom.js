/**
 * Custom scripts needed for the colorpicker, image button selectors,
 * and navigation tabs.
 */

jQuery(document).ready(function($) {

	// Loads the color pickers
	$('.of-color').wpColorPicker();

	// Image Options
	$('.of-radio-img-img').click(function(){
		$(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
		$(this).addClass('of-radio-img-selected');
	});

	$('.of-radio-img-label').hide();
	$('.of-radio-img-img').show();
	$('.of-radio-img-radio').hide();

	// Loads tabbed sections if they exist
	if ( $('.nav-tab-wrapper').length > 0 ) {
		options_framework_tabs();
	}

	function options_framework_tabs() {

		var $group = $('.group'),
			$navtabs = $('.nav-tab-wrapper a'),
			active_tab = '';

		// Hides all the .group sections to start
		$group.hide();

		// Find if a selected tab is saved in localStorage
		if ( typeof(localStorage) != 'undefined' ) {
			active_tab = localStorage.getItem('active_tab');
		}

		// If active tab is saved and exists, load it's .group
		if ( active_tab != '' && $(active_tab).length ) {
			$(active_tab).fadeIn();
			$(active_tab + '-tab').addClass('nav-tab-active');
		} else {
			$('.group:first').fadeIn();
			$('.nav-tab-wrapper a:first').addClass('nav-tab-active');
		}

		// Bind tabs clicks
		$navtabs.click(function(e) {

			e.preventDefault();

			// Remove active class from all tabs
			$navtabs.removeClass('nav-tab-active');

			$(this).addClass('nav-tab-active').blur();

			if (typeof(localStorage) != 'undefined' ) {
				localStorage.setItem('active_tab', $(this).attr('href') );
			}

			var selected = $(this).attr('href');

			$group.hide();
			$(selected).fadeIn();

		});
	}

});



//jQuery(document).ready(function() {
//    if (jQuery('#options-framework-theme-aboutradio1-one').is(':checked')){
//       alert('one');
//    }
//    if (jQuery('#options-framework-theme-aboutradio1-two').is(':checked')){
//       alert('222');
//    }
//})


 

jQuery(document).ready(function() {

        jQuery('#section-about_img1').hide(); 
        jQuery('#aboutifram1').attr('required', true);
        jQuery(":input[name= 'options-framework-theme[aboutradio1]']").on('change', function(){
        if(jQuery('#options-framework-theme-aboutradio1-one').is(":checked"))
        {   //alert('one checked');
             jQuery('#section-aboutifram1').show();
             jQuery('#aboutifram1').attr('required', true);
             jQuery('#about_img1').attr('required', false);
             jQuery('#section-about_img1').hide();     
        }
        if(jQuery('#options-framework-theme-aboutradio1-two').is(":checked"))
        {
           // alert('2 checked');
            jQuery('#section-aboutifram1').hide();
            jQuery('#aboutifram1').attr('required', false);
            jQuery('#about_img1').attr('required', true);
             jQuery('#section-about_img1').show();
        }
      });
      /// one end
      /// two
      if (jQuery('#options-framework-theme-aboutradio2-two').is(':checked')){
          jQuery('#section-iframabout1').hide();
            jQuery('#iframabout1').attr('required', false);
            jQuery('#imageabout2').attr('required', true);
            jQuery('#section-imageabout2').show();
      }
      else{
           jQuery('#section-iframabout1').show();
            jQuery('#iframabout1').attr('required', true);
            jQuery('#imageabout2').attr('required', false);
            jQuery('#section-imageabout2').hide();
      }
       // jQuery('#section-iframabout1').hide();
      //  jQuery('#section-imageabout2').hide();
       //              jQuery('#iframabout1').attr('required', true);
        jQuery(":input[name= 'options-framework-theme[aboutradio2]']").on('change', function(){
        if(jQuery('#options-framework-theme-aboutradio2-one').is(":checked"))
        {   //alert('one2 checked');
            jQuery('#section-iframabout1').show();
            jQuery('#iframabout1').attr('required', true);
            jQuery('#imageabout2').attr('required', false);
            jQuery('#section-imageabout2').hide();
        }
        if(jQuery('#options-framework-theme-aboutradio2-two').is(":checked"))
        {  // alert('22 checked');
            jQuery('#section-iframabout1').hide();
            jQuery('#iframabout1').attr('required', false);
            jQuery('#imageabout2').attr('required', true);
            jQuery('#section-imageabout2').show();
        } 
      });
      
      /// two -- end
      ///threee
      
      jQuery('#section-twoimageabout').hide();
      jQuery('#oneiframabout').attr('required', true);
        jQuery(":input[name= 'options-framework-theme[aboutradio3]']").on('change', function(){
        if(jQuery('#options-framework-theme-aboutradio3-one').is(":checked"))
        {  // alert('one2 checked');
            jQuery('#section-oneiframabout').show();
            jQuery('#oneiframabout').attr('required', true);
            jQuery('#twoimageabout').attr('required', false);
           jQuery('#section-twoimageabout').hide();
        }
        if(jQuery('#options-framework-theme-aboutradio3-two').is(":checked"))
        {  // alert('22 checked');
             jQuery('#section-twoimageabout').show();
            jQuery('#section-oneiframabout').hide();
           jQuery('#oneiframabout').attr('required', false);
           jQuery('#twoimageabout').attr('required', true);

        } 
      });
      
    /// three -end
});




// if (jQuery('#options-framework-theme-aboutradio2-two').is(':checked')){
//        jQuery('#section-aboutifram2').hide();
//        jQuery('#aboutifram1').attr('required', false);
//            jQuery('#about_img1').attr('required', true);
//        jQuery('#section-about_img1').show();
//    }



//     if (jQuery('#options-framework-theme-aboutradio1-one').is(':checked')){
//        jQuery('#section-aboutifram1').show();
//             jQuery('#aboutifram1').attr('required', true);
//             jQuery('#about_img1').attr('required', false);
//             jQuery('#section-about_img1').hide(); 
//    }