<?php
/*
 *  Writesaver View
 */
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_style('admin-responsive-tab-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive-tab.css', '', '', 'all');
wp_enqueue_style('admin-responsive-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive.css', '', '', 'all');
wp_enqueue_style('admin-style_uv-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style_uv.css', '', '', 'all');
wp_enqueue_script('admin-custom-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-diff', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/diff_new.js', array('jquery'), '', 'all');
global $wpdb;

$doc_id = $_GET['doc_id'];
if (empty($doc_id)) {
    print('<script>window.location.href="admin.php?page=all_document_list"</script>');
    exit;
}
$current_user_id = get_current_user_id();
$document = $wpdb->get_results(" SELECT * FROM wp_customer_document_details AS D     
    INNER JOIN wp_customer_document_main AS M
    ON D.fk_doc_main_id = M.pk_document_id
    where D.pk_doc_details_id= $doc_id AND  D.is_active = 1 AND M.Status =1   LIMIT 1");
    if ($document):
?>
<div class="doc_detail " id="doc_detail">
    <h1><?php echo $document[0]->document_title; ?></h1>
    <table class="table" id="list_table">
        <thead>
            <tr>
                <th>Customer Name</th>
                <th>Status</th>
                <th>Word Start No.</th>
                <th>Word End No.</th>
                <th>Total Word</th>
                <th>Assigned Proofreader Name for Single Check</th>
                <th>Assigned Proofreader Name for Double Check</th>               
            </tr>
        </thead>
        <tbody>
            <?php
            $cust_id = $document[0]->fk_customer_id;
            $cust_info = get_userdata($cust_id);

            if ($document[0]->status == 'In Process') {

                $assign_doc = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` where fk_doc_details_id= $doc_id ");
                $single_proof_id = $assign_doc->fk_proofreader_id;
                $single_proof_info = get_userdata($single_proof_id);
            } else {
                $sub_documents = $wpdb->get_results("SELECT * FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id= $doc_id LIMIT 1");
                if (count($sub_documents) > 0) {
                    $single_proof_id = $sub_documents[0]->fk_proofreader_id;
                    $single_proof_info = get_userdata($single_proof_id);
                    $double_proof_id = $sub_documents[0]->Fk_DoubleProofReader_Id;
                    $double_proof_info = get_userdata($double_proof_id);
                }
            }
            ?>
            <tr>               
                <td><a class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $cust_id; ?>"><?php echo $cust_info->first_name . ' ' . $cust_info->last_name; ?></a></td>
                <td class="doc_status"> <?php echo $document[0]->status; ?></td>
                <td><?php echo $document[0]->word_start_no; ?></td>
                <td><?php echo $document[0]->word_end_no; ?></td>
                <td><?php echo $document[0]->word_end_no - $document[0]->word_start_no; ?></td>                            
                <td class="assigned_proof"><a  class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $single_proof_id; ?>"><?php echo $single_proof_info->first_name . ' ' . $single_proof_info->last_name; ?></a>
                    <?php if ($document[0]->status == 'In Process' && $single_proof_id != 0) { ?>
                        <a  href="javascript:void(0);" title="Unassign" data-doc_id="<?php echo $doc_id; ?>" data-proof_id="<?php echo $single_proof_id; ?>" class="unassign_doc"><i class="fa fa-close" aria-hidden="true"></i></a>                      
                        <input type="hidden" value="<?php echo $single_proof_id ?>" id="single_proof_id" />
                    <?php } ?>
                </td>
                <td class="assigned_proof"><a class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $double_proof_id; ?>"><?php echo $double_proof_info->first_name . ' ' . $double_proof_info->last_name; ?></a>
                    <?php if ($document[0]->status == 'Single Check' && $double_proof_id != 0) { ?>
                        <a href="javascript:void(0);" title="Unassign" data-doc_id="<?php echo $doc_id; ?>" data-proof_id="<?php echo $double_proof_id; ?>" class="unassign_doc"><i class="fa fa-close" aria-hidden="true"></i></a>                      
                        <input type="hidden" value="<?php echo $double_proof_id ?>" id="double_proof_id" />
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="main_editor container">
        <form id="frmSubmitedDoc" name="frmSubmitedDoc">
            <div class="editor_top">
                <div class="editor_inner_top">                            
                    <div class="used_word">
                        <span>Used Words:</span>&nbsp;<span class="count">0</span>
                    </div>
                </div>
                <div class="hidden_scroll check">
                    <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">
                        <?php
                        $main_doc_id = $document[0]->fk_doc_main_id;
                        $all_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $main_doc_id AND is_active = 1");
                        
                        // EVNE, DM
                        $orig_doc = $document[0]->document_desc;

                        ?>
                            <div id="oringinal_doc" style="display: none !important;"><?php echo $orig_doc; ?></div>
                            <script src="<?php echo get_template_directory_uri() ?>/js/diff_new.js"></script>
                            <style>
                                .result difftag.diff.ins,
                                .result difftag.diff.del{
                                    font-family: inherit;
                                    display: inline !important;
                                }
                                .result difftag.diff.ins{
                                    background-color: #d6f0d7;
                                }
                                .result difftag.diff.del{
                                    background-color: #fadad7;
                                }
                                .result difftag.diff.ins,
                                .result difftag.diff.del{
                                    position: relative;
                                    display: initial;
                                }
                                .result difftag.diff.ins:hover:before,
                                .result difftag.diff.del:hover:before{
                                    content: "";
                                    width: 0;
                                    height: 0;
                                    display: inline-block;
                                    position: absolute;
                                    border-bottom: 10px solid #d6f0d7;
                                    border-left: 10px solid transparent;
                                    border-right: 10px solid transparent;
                                    left: 0;
                                    z-index: 2;
                                    bottom: -10px;
                                }
                                .result difftag.diff.ins:hover:before{
                                    border-bottom: 10px solid #d6f0d7;
                                }
                                .result difftag.diff.del:hover:before{
                                    border-bottom: 10px solid #fadad7;
                                }
                                .result difftag.diff.ins:hover:after,
                                .result difftag.diff.del:hover:after{
                                    position: absolute;
                                    z-index: 1;
                                    padding: 10px;
                                    background-color: #fff;
                                    box-shadow: 1px 1px 2px 1px #ddd;
                                    text-align: center;
                                    width: 125px;
                                    transform: translateY(100%);
                                    bottom: -10px;
                                    left: -10px;
                                }
                                .result difftag.diff.ins:hover:after{
                                    content:"New Word";
                                }
                                .result difftag.diff.del:hover:after{
                                    content:"Delete Word";
                                }
                            </style>
                            <script>
                            //EVNE, DM
                            jQuery(document).ready(function(){
                                var origin_desc = jQuery('#oringinal_doc').text().trim();
                                var double_desc = jQuery('.changed_doc').text().trim();
                                var track_result = displayDiffString(origin_desc, double_desc);
                                track_result = track_result.trim().replace(/\n\n/g, "\n");
                                track_result = track_result.replace(/\n/g, "<br>");
                                //track_result = track_result.replace(/&amp;/, "&");
                                //track_result = track_result.replace(/&nbsp;/, " ");
                                jQuery('div.result .parentscrollcontents .changeable.check').html(track_result);
                            });
                            </script>
                        <?php
                        foreach ($all_doc as $doc) {
                            $sub_document_id = $doc->pk_doc_details_id;
                            $status_1 = $doc->status;
                            if ($doc->status == 'Pending') {
                                $single_proof = '';
                                $double_proof = '';
                            } elseif ($doc->status == 'In Process') {
                                $assign_doc = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` where fk_doc_details_id= $sub_document_id");
                                $single_proof_id = $assign_doc->fk_proofreader_id;
                                $single_proof_info = get_userdata($single_proof_id);
                                $single_proof = $single_proof_info->first_name . ' ' . $single_proof_info->last_name;
                                $double_proof = '';
                            } else {
                                $sub_documents = $wpdb->get_results("SELECT * FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id= $sub_document_id LIMIT 1");

                                if (count($sub_documents) > 0) {
                                    $single_proof_id = $sub_documents[0]->fk_proofreader_id;
                                    $single_proof_info = get_userdata($single_proof_id);
                                    $single_proof = $single_proof_info->first_name . ' ' . $single_proof_info->last_name;
                                    $double_proof_id = $sub_documents[0]->Fk_DoubleProofReader_Id;
                                    if ($double_proof_id != NULL) {
                                        $double_proof_info = get_userdata($double_proof_id);
                                        $double_proof = $double_proof_info->first_name . ' ' . $double_proof_info->last_name;
                                    } else
                                        $double_proof = '';
                                }
                            }
                            if ($double_proof && $double_proof != NULL)
                                $proof_name = $double_proof;
                            else
                                $proof_name = $single_proof;
                            ?>

                            <div data-proof_name ="<?php echo $proof_name; ?>" class="ori changeable check <?php echo ($doc->pk_doc_details_id == $document[0]->pk_doc_details_id ? "" : "not_editable"); ?>" contenteditable="<?php echo ($doc->pk_doc_details_id == $document[0]->pk_doc_details_id ? "true" : "false"); ?>" id="<?php echo ($doc->pk_doc_details_id == $document[0]->pk_doc_details_id ? "txt_area_upload_doc" : "txt_area_upload_doc_" . $doc->pk_doc_details_id); ?>" data-id="<?php echo $doc->pk_doc_details_id; ?>" style="background-color: <?php echo ($doc->pk_doc_details_id == $document[0]->pk_doc_details_id ? "white" : "#f5f5f5"); ?>; " >
                                <?php
                                if ($status_1 == "Pending" || $status_1 == "In Process") {
                                    $output = trim($doc->document_desc);
                                    $output = str_replace("\n", "<p></p>", $output);
                                    echo trim($output);
                                } else {

                                    $Proofreaded_doc1 = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details where fk_doc_details_id= $sub_document_id");
                                    $status_doc = $Proofreaded_doc1[0]->status;
                                    ?>
<div class="changed_doc" style="display:none !important;"><?php
$changed_doc = $Proofreaded_doc1[0]->double_doc_desc;
echo stripslashes($changed_doc); ?></div>
                                    <?php
                                    if ($status_doc == 'Completed') {
                                        $output = trim(trim($Proofreaded_doc1[0]->double_doc_desc));
                                        //$output = str_replace("\n", "<br>", $output);
                                        //$output = preg_replace('/\.[\s]{2,}/', '', $output);
                                        echo stripslashes($output);
                                    } else {
                                        $output = trim($Proofreaded_doc1[0]->doc_desc);
                                        //$output = str_replace("\n", "<br>", $output);
                                        //$output = preg_replace('/\.[\s]{2,}/', '', $output);
                                        echo stripslashes($output);
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="<?php echo $document[0]->pk_doc_details_id; ?>" />
            <?php if ($document[0]->status != 'Completed') { ?>
                <div class="submit_area">
                    <div class="btn_blue">                        
                        <a href="javascript:void(0);" class="btn_sky" id="btnFinishedMySection" <?php echo ($document[0]->status != 'Single Check') ? '' : 'style="display: none;"'; ?>>Finished my section</a>
                        <?php //if ($single_proof_id != $current_user_id) :   ?>
                        <a href="javascript:void(0);" class="btn_sky" id="btnLooksPerfect" <?php echo ($document[0]->status == 'Single Check') ? '' : 'style="display: none;"'; ?> >Looks Perfect!</a>
                        <?php //endif;  ?>
                        <input type="hidden" id="hdnMainDocId" name="hdnMainDocId" value="<?php echo $document[0]->fk_doc_main_id; ?>">
                        <input type="hidden" id="hdnSubDocId" name="hdnSubDocId" value="<?php echo $document[0]->pk_doc_details_id; ?>">
                        <input type="hidden" id="hdnCustomerId" name="hdnCustomerId" value="<?php echo $document[0]->fk_cust_id; ?>">
                    </div>
                </div>
            <?php } ?>
        </form>
    </div>
    <!-- EVNE, DM -->
    <style>
        .editions ins{
            background-color:#d6f0d7;
        }
        .editions del{
            background-color:#fadad7;
        }
    </style>
    <h2 style="text-align: center;margin:20px 0;">Editions</h2>
    <div class="main_editor container editions">
        <form id="frmSubmitedDoc" name="frmSubmitedDoc">
            <div class="editor_top">
                <div class="hidden_scroll check result">
                    <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">
                        <div class="changeable check"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!--POPUP-->

<div id="finish_section" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="load_overlay pop_loding" >
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <div class="page_title">
                <h2>Are you sure to complete proofreading of the document? </h2>
            </div>
            <div class="pop_content">
                <div class="first_test save_doc">
                    <p>Are you sure to complete proofreading of the document? You will not be able to access your submission again.</p>                       
                    <a href="javascript:void(0);" class="btn_sky btnyes">Yes</a>
                    <a href="javascript:void(0);" class="btn_sky btnno  orange pop_btn">No</a>                       
                </div>
                <div class="doc_msg"></div>
            </div>
        </div>
    </div>
</div>

<div id="look_perfect" class="pop_overlay open start_tests ready" style="display: none;">

    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="load_overlay pop_loding" >
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <div class="page_title">
                <h2>Are you sure to complete proofreading of the document? </h2>
            </div>
            <div class="pop_content">
                <div class="first_test save_doc">
                    <p>Are you sure to complete proofreading of the document? You will not be able to access your submission again.</p>                       
                    <a href="javascript:void(0);" class="btn_sky btnyes">Yes</a>
                    <a href="javascript:void(0);" class="btn_sky btnno  orange pop_btn">No</a>                       
                </div>
                <div class="doc_msg"></div>
            </div>
        </div>
    </div>
</div>
<?php
else:
   echo '<h3>No Document Detail Found...</h3>';
    endif;

?>
<script>
    jQuery('.unassign_doc').live('click', function (e) {
        var button = jQuery(this);
        var doc_id = jQuery(this).attr('data-doc_id');
        var proof_id = jQuery(this).attr('data-proof_id');
        var doc_status = button.parents('tr').find('td.doc_status').text();
        if (doc_id && proof_id) {
            var r = confirm("Are you sure to unassign proofreader?");
            if (r == true) {
                jQuery('#loding').show();
                jQuery.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'unassign_proofreader',
                        doc_id: doc_id,
                        proof_id: proof_id,
                        doc_status: doc_status
                    },
                    success: function (data) {
                        jQuery('#loding').hide();
                        if (data == 0) {
                            button.after('<span class="text-danger statusmsg">Not unassigend sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(5000);
                        } else {
                            button.after('<span class="text-success statusmsg">Unassigend sucessfully... </span>');
                            window.setTimeout(function () {
                                jQuery(".statusmsg").fadeOut(7000);
                                location.reload();
                            }, 2000);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        jQuery('.pop_loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            } else {
                $(r).dialog("close");
            }
        }
    });
    // Get Word count
    function get_doc_word_count(val) {
        var wom = val.match(/\S+/g);
        return wom ? wom.length : 0;
    }

    jQuery(document).ready(function () {

        var doc_detail = jQuery('#txt_area_upload_doc').text().trim();
        //doc_detail = doc_detail.replace(/\s+/g, ' ');
        var words = get_doc_word_count(doc_detail);
        jQuery(".used_word .count").text(words);

        var main = $(".hidden_scroll");
        t = main.offset().top + 25;
        $('.parentscrollcontents').animate({
            scrollTop: $("#txt_area_upload_doc").offset().top - t
        }, 2000);

        $('.not_editable').on({
            mouseenter: function () {
                $(".popup_text").remove();
                var proof_name = $(this).attr('data-proof_name');
                if (proof_name.trim() != '') {
                    $('.hidden_scroll.check').append('<span class="popup_text">Proofread by ' + proof_name + '</span>');
                    var p = $(this);
                    var offset = p.offset();
                    var offset_top = offset.top;
                    if (offset_top < 0) {
                        $('.popup_text').css('top', 90);
                    } else {
                        $('.popup_text').css('top', offset.top / 3);
                    }
                }
            },
            mouseleave: function () {
                $(".popup_text").remove();
            }
        });

//        jQuery('#txt_area_upload_doc').keydown(function (e) {
//            if (e.keyCode == 13) {
//                document.execCommand('insertHTML', false, ' ');
//            }
//        });

        jQuery('#txt_area_upload_doc').bind("DOMNodeInserted", function () {
            if (jQuery(this).text().trim() == "")
            {
                jQuery(".used_word .count").text("0");
            } else
            {
                var doc_detail = jQuery(this).text().trim();
                var words = get_doc_word_count(doc_detail)
                jQuery(".used_word .count").text(words);
            }
        });

        jQuery('#txt_area_upload_doc').keyup(function () {
            if (jQuery(this).text().trim() == "")
            {
                jQuery(".used_word .count").text("0");
            } else
            {
                var doc_detail = jQuery(this).text().trim();
                var words = get_doc_word_count(doc_detail)
                jQuery(".used_word .count").text(words);
            }
        });

        $("#btnFinishedMySection").click(function () {
            var desc = $("#txt_area_upload_doc").text();
            if (desc != "") {
                $('#finish_section').fadeIn();
                $('#finish_section').addClass('open');
            } else
            {
                $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
                setTimeout(function () {
                    $('.spn_submited_doc_error').fadeOut('slow');
                }, 2000);
            }
            return  false;
        });

        $('#finish_section .pop_head a .fa-remove,#finish_section .save_doc .btnno').click(function (e) {
            event.stopPropagation();
            $('#finish_section').fadeOut();
            $('#finish_section').removeClass('open');
        });

        $('#finish_section .save_doc .btnyes').click(function (e) {
            $('.docmsg').remove();
            var desc = $('#txt_area_upload_doc').html().trim();
            desc = desc.replace(/<br>/g, "\n");
            desc = desc.replace(/<div>/g, "");
            desc = desc.replace(/<\/div>/g, "\n");
            
            var fk_cust_id = $("#hdnCustomerId").val();
            var fk_main_doc_id = $("#hdnMainDocId").val();
            var fk_sub_doc_id = $("#hdnSubDocId").val();
            var docid = $("#hdndocidpartsid").val();
            var changed_wrod_ary = get_diff_str_array(doc_detail, desc);
            var single_proof_id = $("#single_proof_id").val();
            if (desc != "")
            {
                $('.pop_loding').show();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    method: "post",
                    data: {
                        action: 'submited_doc_by_proofreader',
                        doc_id: docid,
                        changed_wrod_ary: changed_wrod_ary,
                        word_desc: desc,
                        fk_cust_id: fk_cust_id,
                        fk_main_doc_id: fk_main_doc_id,
                        fk_sub_doc_id: fk_sub_doc_id,
                        user_role: 'admin',
                        single_proof_id: single_proof_id
                    },
                    type: 'POST',
                    success: function (data) {
                        if (data == 'error')
                        {
                            $("#btnFinishedMySection").css("display", "");
                            $("#btnLooksPerfect").css("display", "none");
                            $("#txt_area_upload_doc").attr("contenteditable", "true");
                            $("#txt_area_upload_doc").removeClass("proofreaderdesc");
                            $('.pop_loding').hide();
                        } else
                        {

                            $(".doc_msg").html('<span class="text-success docmsg">Document submitted successfully...</span>');
                            $("#btnFinishedMySection").css("display", "");
                            $("#btnLooksPerfect").css("display", "none");
                            $("#txt_area_upload_doc").attr("contenteditable", "true");
                            $('.pop_loding').hide();
                            window.setTimeout(function () {
                                location.reload();
                            }, 1000)
                        }
                    }
                    ,
                    error: function (jqXHR, textStatus, errorThrown) {
                        jQuery('.pop_loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                }
                );
                return false;
            } else
            {
                $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
                setTimeout(function () {
                    $('.spn_submited_doc_error').fadeOut('slow');
                }, 2000);
            }
        });

        $("#btnLooksPerfect").click(function () {
            $('.double_msg').remove();
            $('#look_perfect').fadeIn();
            $('#look_perfect').addClass('open');
            return false;
        });

        $('#look_perfect .pop_head a .fa-remove, #look_perfect .save_doc .btnno').click(function (e) {
            event.stopPropagation();
            $('#look_perfect').fadeOut();
            $('#look_perfect').removeClass('open');
        });

        $('#look_perfect .save_doc .btnyes').click(function (e) {
            $('.docmsg').remove();
            $('.pop_loding').show();

            var desc = $('#txt_area_upload_doc').html().trim();
            desc = desc.replace(/<br>/g, "\n");
            desc = desc.replace(/<div>/g, "");
            desc = desc.replace(/<\/div>/g, "\n");


            var fk_cust_id = $("#hdnCustomerId").val();
            var fk_main_doc_id = $("#hdnMainDocId").val();
            var fk_sub_doc_id = $("#hdnSubDocId").val();
            var docid = $("#hdndocidpartsid").val();
            var changed_wrod_ary = get_diff_str_array(doc_detail, desc);
            var double_proof_id = $("#double_proof_id").val();

            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                method: "post",
                data: {
                    user_role: 'admin',
                    double_proof_id: double_proof_id,
                    action: 'updateDocStatus',
                    doc_id: docid,
                    changed_wrod_ary: changed_wrod_ary,
                    word_desc: desc,
                    fk_cust_id: fk_cust_id,
                    fk_main_doc_id: fk_main_doc_id,
                    fk_sub_doc_id: fk_sub_doc_id
                },
                success: function (data2) {

                    if (data2 != 0) {
                        $(".doc_msg").html('<span class="text-success docmsg">Document submitted successfully...</span>');
                        window.setTimeout(function () {
                            $('#look_perfect').fadeOut();
                            $('#look_perfect').removeClass('open');
                            $('#pop_start_Confirm').fadeIn('slow');
                            location.reload();
                        }, 1000);
                    } else {
                        $(".doc_msg").html('<span class="text-danger docmsg">Please try again.</span>');
                        window.setTimeout(function () {
                            $('#look_perfect').fadeOut();
                            $('#look_perfect').removeClass('open');
                            $('.double_msg').fadeOut('slow');
                        }, 1000);
                    }
                    $('.pop_loding').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.pop_loding').hide();
                    $('#look_perfect').fadeOut('slow');
                    $('#look_perfect').removeClass('open');
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
        });
    });


</script>