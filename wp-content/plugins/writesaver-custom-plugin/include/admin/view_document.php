<?php
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');

wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_style('admin-responsive-tab-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive-tab.css', '', '', 'all');
wp_enqueue_style('admin-responsive-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive.css', '', '', 'all');
wp_enqueue_style('admin-style_uv-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style_uv.css', '', '', 'all');
wp_enqueue_script('admin-custom-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
global $wpdb;

$doc_id = $_GET['doc_id'];
if (empty($doc_id)) {
    print('<script>window.location.href="admin.php?page=all_document_list"</script>');
    exit;
}

$documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` where pk_document_id= $doc_id AND Status =1 LIMIT 1");
if ($documents) {

    $cust_id = $documents[0]->fk_customer_id;
    $cust_info = get_userdata($cust_id);

    $sub_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` WHERE fk_doc_main_id= $doc_id AND is_active = 1");
    ?>
    <div class="doc_detail" id="doc_detail">
        <h1><?php echo $documents[0]->document_title . ' - ' . $cust_info->first_name . ' ' . $cust_info->last_name; ?></h1>
        <table class="table" id="list_table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sub Doc Number</th>
                    <th>Status</th>
                    <th>Word Start No.</th>
                    <th>Word End No.</th>
                    <th>Total Word</th>
                    <th>Proofreader for Single Check</th>
                    <th>Time to Completion for Single Check</th>
                    <th>Proofreader for Double Check</th>
                    <th>Time to Completion for Double Check</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $doc_count = 0;
                foreach ($sub_documents as $sub_document) {
                    $doc_count++;
                    $sub_document_id = $sub_document->pk_doc_details_id;

                    $single_start_time = $sub_document->single_start_time;
                    $single_end_time = $sub_document->single_end_time;
                    $double_start_time = $sub_document->double_start_time;
                    $double_end_time = $sub_document->Completed_date;

                    if ($sub_document->status == 'Pending') {
                        $single_proof_info = '';
                        $double_proof_info = '';
                    } elseif ($sub_document->status == 'In Process') {
                        $assign_doc = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` where fk_doc_details_id= $sub_document_id");
                        $single_proof_id = $assign_doc->fk_proofreader_id;
                        $single_proof_info = get_userdata($single_proof_id);
                        $double_proof_info = '';
                    } else {
                        $sub_documents = $wpdb->get_results("SELECT * FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id= $sub_document_id LIMIT 1");

                        if (count($sub_documents) > 0) {
                            $single_proof_id = $sub_documents[0]->fk_proofreader_id;
                            $single_proof_info = get_userdata($single_proof_id);
                            $double_proof_id = $sub_documents[0]->Fk_DoubleProofReader_Id;
                            $double_proof_info = get_userdata($double_proof_id);
                        }
                    }
                    ?>

                    <tr>
                        <td><?php echo $doc_count; ?></td>
                        <td><?php echo 'Sub Doc ' . $doc_count ?></td>
                        <td><?php echo $sub_document->status; ?></td>
                        <td><?php echo $sub_document->word_start_no; ?></td>
                        <td><?php echo $sub_document->word_end_no; ?></td>
                        <td><?php echo $sub_document->word_end_no - $sub_document->word_start_no; ?></td>                            
                        <td>
                            <?php if ($single_proof_info): ?>
                                <a   class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $single_proof_id; ?>"><?php echo $single_proof_info->first_name . ' ' . $single_proof_info->last_name; ?></a>
                            <?php endif; ?>
                        </td>
                        <td><?php
                            if ($single_start_time && $single_end_time) {
                                echo date_getFullTimeDifference($single_start_time, $single_end_time);
                            }
                            ?></td>
                        <td>
                            <?php if ($double_proof_info): ?>
                                <a  class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $double_proof_id; ?>"><?php echo $double_proof_info->first_name . ' ' . $double_proof_info->last_name; ?></a>
                            <?php endif; ?>
                        </td>
                        <td><?php
                            if ($double_start_time && $double_end_time) {
                                echo date_getFullTimeDifference($double_start_time, $double_end_time);
                            }
                            ?></td>
                        <td><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=view_sub_document&doc_id=<?php echo $sub_document_id; ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
} else {
    echo '<h3>No Document Detail Found...</h3>';
}
?>
<script>
    jQuery(document).ready(function () {
        jQuery('#list_table').DataTable({
            "oLanguage": {
                "sEmptyTable": "No document available."
            }
        });
    });
</script>
