<?php

/*
 * Description : Main file to manage function for writesaver function custom
 */

/*
 *  Menu management
 */

function writesaver_menu_setup() {

    add_menu_page('Writesaver custom (By Aliansoftware)', 'Writesaver Custom', '', 'writesaver-form', 'WritesaverForm');

    add_submenu_page('writesaver-form', 'Proofreader List', 'Proofreader List', 'manage_options', 'proofreader_list', 'add_proofreader_mainmenu');
    add_submenu_page('writesaver-form', 'Payment Requests', 'Payment Requests', 'manage_options', 'payment_requests', 'add_payment_requests');
    add_submenu_page('writesaver-form', 'All Document List', 'All Document List', 'manage_options', 'all_document_list', 'add_all_document_list');
    add_submenu_page('writesaver-form', 'Recurring words', 'Recurring words', 'manage_options', 'recurring_words', 'add_recurring_words');
    add_submenu_page('', 'Proofreader View', 'Proofreader View', 'manage_options', 'proofreader_view', 'add_proofreader_view');
    add_submenu_page('', 'View User', 'View User', 'manage_options', 'view_user', 'add_user_view');
    add_submenu_page('', 'View Documents', 'View Documents', 'manage_options', 'view_documents', 'add_documents_view');
    add_submenu_page('', 'View Sub Document', 'View Sub Document', 'manage_options', 'view_sub_document', 'add_sub_document_view');
}

add_action('admin_menu', 'writesaver_menu_setup');

add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');

function load_custom_wp_admin_style() {
    wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
    wp_enqueue_style('admin-customs-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/admin_style.css', '', '', 'all');
}

function add_proofreader_mainmenu() {
    include_once( plugin_dir_path(__FILE__) . '/admin/proofreader_list.php' );
}

function add_proofreader_view() {
    include_once( plugin_dir_path(__FILE__) . '/admin/proofreader_view.php' );
}

function add_payment_requests() {
    include_once( plugin_dir_path(__FILE__) . '/admin/payment_requests.php' );
}

function add_user_view() {
    include_once( plugin_dir_path(__FILE__) . '/admin/view_user.php' );
}

function add_all_document_list() {
    include_once( plugin_dir_path(__FILE__) . '/admin/all_document_list.php' );
}

function add_documents_view() {
    include_once( plugin_dir_path(__FILE__) . '/admin/view_document.php' );
}

function add_sub_document_view(){
     include_once( plugin_dir_path(__FILE__) . '/admin/view_sub_document.php' );
 }

function add_recurring_words(){
     include_once( plugin_dir_path(__FILE__) . '/admin/recurring_words.php' );
}