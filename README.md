# README #

This is the repository for writesaver.co. All changes should branch off of the "dev" branch right now. The 'master' branch is only for code that will deploy immediately to the live website. I want to look over everything that goes into the 'master' branch before it gets pushed (mostly to check for English mistakes in text and error messages since it's a proofreading site :)). 

**Pushing to the development site**

There is a remote set up, "devserver". When you push the "staging" branch to this remote, it will automatically deploy to staging.writesaver.co. 


I will do all the pushes to the the live production site, after I check the English. Feel free to push changes to the staging site whenever you have new features you'd like me to look at.


**Branches**

Branch names should be descriptive in saying what they are for. New features should start with the word "feature", and bug fixes with "bugfix".

The 'master', 'staging' and 'dev' branches should never be deleted. Feature and bug fix branches can be deleted after their contents are merged with the relevant branch. 

The 'staging' branch should be the primary branch for development.